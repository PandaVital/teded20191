package br.ucsal.bes20191.ed.tedDeEd.dao;

import java.util.Scanner;

import br.ucsal.bes20191.ed.tedDeEd.Model.Produto;

public class Vendas {
	private static Estoque estoque = new Estoque();
	public static Scanner sc = new Scanner(System.in);

	public void inseriProduto(Produto produto) {
		
			estoque.cadastraProduto(produto);
			
		
	}

	public void listaDeVendas(Produto produto, int qtd) {
		if (estoque.consultarProduto(produto)) {
			System.out.println("Nome do produto comprado :"+produto.getNome());
			Double valor = produto.getPreco();
			System.out.println("valor da unidade : R$"+valor);
			Double valorTotal = valor * qtd;
			System.out.println("O valor de " + qtd + " " + produto.getNome() + " = " + valorTotal);
		}
	}

	public int vender(Produto produto) {
		// TODO Auto-generated method stub

		// deve conter todos os produtos contidos na mesma, as quantidades e
		// valores individuais e totais por produto,
		// bem como o valor total da venda.

		if (estoque.consultarProduto(produto)) {
			System.out.println("informe a quantidade de " + produto.getNome() + " ser�o vendidos");
			int qtd = sc.nextInt();
			estoque.removerProduto(qtd, produto);
			return qtd;
		} else {
			System.out.println("Esse produto nao pode ser vendido");
		}
		return 0;

	}

	public void cancelarVenda(Produto produto, int qtd) {
		// TODO Auto-generated method stub
		// Ao cancelar uma venda os produtos contidos na mesma dever�o ser
		// retornados ao estoque.
		if (estoque.consultarProduto(produto) == true) {
			int quantidadeAtual = produto.getQuantidade() + qtd;
			produto.setQuantidade(quantidadeAtual);
		}
		
			
			
		}
	public void informacaoEstoque(){
		System.out.println("---------------------Estoque---------------------");
		estoque.imprimirEstoque();
		System.out.println("------------------------------------------");

	}

}
