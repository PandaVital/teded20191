package br.ucsal.bes20191.ed.tedDeEd.dao;

import br.ucsal.bes20191.ed.tedDeEd.Model.Produto;

public class Estoque {
	No inicio = null;

	public void cadastraProduto(Produto novoProduto) {
		No novo = new No();
		novo.produto = novoProduto;
		if (inicio == null) {
			inicio = novo;
		} else {
			No aux = inicio;
			while (aux.prox != null) {
				aux = aux.prox;
			}
			aux.prox = novo;
		}

	}

	public void removerProduto(int qtd, Produto produto) {
		No aux = inicio;
		if (inicio == null) {
			System.out.println("lista vazia");
		} else {

			while (aux.prox != null && aux.produto != produto) {
				aux = aux.prox;
			}

		}
		if (aux.produto == produto) {
			int qtdAtual = aux.produto.getQuantidade() - qtd;
			aux.produto.setQuantidade(qtdAtual);
		} else {
			System.out.println("Produto n�o encontrado");
		}

	}

	public boolean consultarProduto(Produto produto) {
		No aux = inicio;
		if (inicio == null) {
			System.out.println("lista vazia");
		} else {

			while (aux.prox != null && aux.produto != produto) {
				aux = aux.prox;
			}

		}
		if (aux.produto == produto) {
			return true;
		} else {
			return false;
		}

	}

	public void excluirProduto(Produto produto) {
		if (inicio == null) {
			System.out.println("lista vazia");
		} else {
			No aux = inicio;
			No ant = null;
			while (aux.prox != null && produto != aux.produto) {
				ant = aux;
				aux = aux.prox;
			}
			if (aux.produto == produto) {
				if (ant == null) {
					inicio = inicio.prox;
				} else if (aux.prox == null) {
					ant.prox = null;
				} else {
					ant.prox = aux.prox;
				}
			} else {
				System.out.println("valor no encontrado");
			}
		}

	}

	public void imprimirEstoque() {
		// Tamb�m dever� ser poss�vel imprimir uma listagem dos produtos com
		// pre�o e quantidade e o valor total
		// do estoque, que ser� a soma dos valores de todos os produtos
		// existentes no estoque
		Double totalPreco = 0d;

		No aux = inicio;
		if (aux.prox == null) {
			System.out.println("lista vazia");
			System.out.println("Total de produtos e pre�o total foi zero");
		}
		while (aux != null) {
			System.out.println(aux.produto.getNome());
			totalPreco = totalPreco + (aux.produto.getPreco() * aux.produto.getQuantidade());

			System.out.println("Quantida de produtos:" + aux.produto.getQuantidade());

			aux = aux.prox;
		}

		System.out.println("Valor total do estoque foi:" + totalPreco);

	}

}
