package br.ucsal.bes20191.ed.tedDeEd.Model;

import java.util.Scanner;

import br.ucsal.bes20191.ed.tedDeEd.dao.Estoque;
import br.ucsal.bes20191.ed.tedDeEd.dao.Vendas;

public class Main {
	public static Scanner sc = new Scanner(System.in);
	private static Vendas vend = new Vendas();

	public static void main(String[] args) {
		Produto produto1 = new Produto("cadeira", 1, "cadeira de metal", TipoEnum.MOVEIS, 70.0, 100);
		vend.inseriProduto(produto1);

		Produto produto2 = new Produto("mesa", 2, "mesa de vidro", TipoEnum.MOVEIS, 900.0, 10);
		vend.inseriProduto(produto2);

		Produto produto3 = new Produto("xbueguer", 3, "pao,queijo,carne e salada", TipoEnum.ALIMENTACAO, 20.0, 100);
		vend.inseriProduto(produto3);

		Produto produto4 = new Produto("cortina", 4, "branca", TipoEnum.DECORACAO, 35.50, 100);
		vend.inseriProduto(produto4);

		int qtdProd1 = vend.vender(produto1);
		int qtdProd2 = vend.vender(produto2);
		int qtdProd3 = vend.vender(produto3);
		int qtdProd4 = vend.vender(produto4);

		vend.listaDeVendas(produto1, qtdProd1);
		vend.listaDeVendas(produto2, qtdProd2);
		vend.listaDeVendas(produto3, qtdProd3);
		vend.listaDeVendas(produto4, qtdProd4);
		
		vend.cancelarVenda(produto4, qtdProd4);
		vend.informacaoEstoque();
		

	}

}
