package br.ucsal.bes20191.ed.tedDeEd.Model;

public class Produto {

	private String nome;

	private Integer codigo;

	private String descricao;

	private TipoEnum tipo;

	private Double preco;
	
	public int quantidade;

	public Produto(String nome, Integer codigo, String descricao, TipoEnum tipo, Double preco,int quantidade) {
		super();
		this.nome = nome;
		this.codigo = codigo;
		this.descricao = descricao;
		this.tipo = tipo;
		this.preco = preco;
		this.quantidade=quantidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoEnum tipo) {
		this.tipo = tipo;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	

	@Override
	public String toString() {
		return "Produto [nome=" + nome + ", codigo=" + codigo + ", descricao=" + descricao + ", tipo=" + tipo
				+ ", preco=" + preco + ", quantidade=" + quantidade + "]";
	}
}
